const namespaced = true
import { STORAGE_KEYS } from "@/core/constants"
import Vue from "vue";

const Store = require('electron-store');
const store = new Store();

const state = {
    projectScripts: {},
    globalScripts: [],
    activeGScripts: {},

}

const getters = {
    projectScriptList: state => {
        return state.projectScripts;
    }
}

const mutations = {
    GET_GLOBAL_SCRIPTS(state) {
        state.globalScripts = store.get(STORAGE_KEYS.SCRIPTS_KEY) || [];
    },

    ADD_GLOBAL_SCRIPT(state, scriptData) {
        state.globalScripts.push(scriptData)
        store.set(STORAGE_KEYS.SCRIPTS_KEY, state.globalScripts)
    },

    DELETE_GLOBAL_SCRIPT(state, scriptId) {
        state.globalScripts = state.globalScripts.filter(({ id }) => id !== scriptId)
        store.set(STORAGE_KEYS.SCRIPTS_KEY, state.globalScripts)
    },

    ADD_ACTIVE_GLOBAL_SCRIPT(state, data) {
        Vue.set(state.activeGScripts, data.pid, data)
    },
    DELETE_ACTIVE_GLOBAL_SCRIPT(state, pid) {
        Vue.delete(state.activeGScripts, pid)
    },
    UPDATE_ACTIVE_GLOBAL_SCRIPT_OUTPUT(state, data) {
        state.activeGScripts[data.pid].output.push(data.output)
    },

    ADD_PROJECT_SCRIPT(state, data) {
        Vue.set(state.projectScripts, data.pid, data)
    },

    UPDATE_PROJECT_SCRIPT_OUTPUT(state, data) {
        // console.log(data.output);
        if (!data.output.includes('[webpack.Progress]') && (!data.output.includes('') && !data.output.includes('%'))) {
            state.projectScripts[data.pid].output.push(data.output)
        }
    },
    DELETE_PROJECT_SCRIPT(state, pid) {
        Vue.delete(state.projectScripts, pid)
    }
}

const actions = {
    getGlobalScripts({ commit }) {
        commit('GET_GLOBAL_SCRIPTS')

    },
    addGlobalScript({ commit }, data) {
        commit('ADD_GLOBAL_SCRIPT', data);
    },
    deleteGlobalScript({ commit }, data) {
        commit('DELETE_GLOBAL_SCRIPT', data);
    },
    addActiveGlobalScript({ commit }, data) {
        commit('ADD_ACTIVE_GLOBAL_SCRIPT', data)
    },
    deleteActiveGlobalScript({ commit }, data) {
        commit('DELETE_ACTIVE_GLOBAL_SCRIPT', data)
    },
    updateActiveGlobalScriptOutput({ commit }, data) {
        commit('UPDATE_ACTIVE_GLOBAL_SCRIPT_OUTPUT', data)
    },
    addProjectScript({ commit }, data) {
        commit('ADD_PROJECT_SCRIPT', data)
    },
    updateProjectScriptOutput({ commit }, data) {
        commit('UPDATE_PROJECT_SCRIPT_OUTPUT', data)
    },
    deleteProjectScript({ commit }, data) {
        commit('DELETE_PROJECT_SCRIPT', data)
    }
}

export default {
    getters,
    namespaced,
    state,
    mutations,
    actions
}