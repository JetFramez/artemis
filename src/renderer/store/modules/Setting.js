const namespaced = true
import { STORAGE_KEYS } from "@/core/constants"
import Vue from "vue";

const Store = require('electron-store');
const store = new Store();

const state = {
    settings: {},


}

const getters = {

}

const mutations = {
    GET_SETTINGS(state) {
        // Does settings json exist?
        if (store.has(STORAGE_KEYS.SETTINGS_KEY)) { // YES
            state.settings = store.get(STORAGE_KEYS.SETTINGS_KEY)
        } else { //NO
            // SET DEFAULT VALUES
            state.settings = {
                terminal: 'wt',
                node_package_manager: 'yarn',
                ide: 'vscode'
            }

            store.set(STORAGE_KEYS.SETTINGS_KEY, state.settings);
        }
    },

    SET_SETTINGS(state, { key, value }) {
        // Object.assign(state.settings, data)
        Vue.set(state.settings, key, value);
        store.set(STORAGE_KEYS.SETTINGS_KEY, state.settings);

    },

}

const actions = {
    getSettings({ commit }) {
        commit('GET_SETTINGS')
    },
    setSettings({ commit }, data) {
        commit('SET_SETTINGS', data)
    }
}

export default {
    getters,
    namespaced,
    state,
    mutations,
    actions
}