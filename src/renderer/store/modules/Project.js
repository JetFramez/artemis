const namespaced = true
import { STORAGE_KEYS } from "@/core/constants"

const Store = require('electron-store');
const store = new Store();

const state = {
    projects: []
}

const mutations = {
    GET_PROJECTS(state) {
        state.projects = store.get(STORAGE_KEYS.PROJECTS_KEY) || [];
        // console.log(store.path)
    },

    ADD_PROJECT(state, projectData) {

        state.projects.push(projectData)
        store.set(STORAGE_KEYS.PROJECTS_KEY, state.projects)
    },
    DELETE_PROJECT(state, projectId) {
        state.projects = state.projects.filter(({ id }) => id !== projectId)
        store.set(STORAGE_KEYS.PROJECTS_KEY, state.projects)
    }
}

const actions = {
    getProjects({ commit }) {
        commit('GET_PROJECTS')

    },
    addProject({ commit }, data) {
        commit('ADD_PROJECT', data);
    },
    deleteProject({ commit }, data) {
        commit('DELETE_PROJECT', data);
    }
}

export default {
    namespaced,
    state,
    mutations,
    actions
}