import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
    routes: [{
            path: '/',
            name: 'Projects',
            component: require('@/views/Projects.vue').default
        },
        {
            path: '/scripts',
            name: 'Scripts',
            component: require('@/views/Scripts.vue').default
        },
        {
            path: '/settings',
            name: 'Settings',
            component: require('@/views/Settings.vue').default
        },
        {
            path: '*',
            redirect: '/'
        }
    ]
})