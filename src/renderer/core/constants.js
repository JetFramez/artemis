const STORAGE_KEYS = {
    PROJECTS_KEY: 'artemis_projects',
    SCRIPTS_KEY: 'artemis_scripts',
    SETTINGS_KEY: 'artemis_settings'
}


module.exports = {
    STORAGE_KEYS
}